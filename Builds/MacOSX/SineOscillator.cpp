//
//  SineOscillator.cpp
//  JuceBasicAudio
//
//  Created by Jeffrey Bateman on 11/19/14.
//
//

#include "SineOscillator.h"
#include "math.h"

SineOscillator::SineOscillator()
{
    twoPi = 2 * M_PI;
    frequency = 0.f;
    amplitude = 0.f;
    phaseIncrement = 0.f;
    
}

SineOscillator::~SineOscillator()
{
    
}

float SineOscillator::getSample()
{
    float phase;
    phaseIncrement = (twoPi * frequency)/sampleRate;
    phasePosition += phaseIncrement;
    if (phasePosition > twoPi)
        phasePosition -= twoPi;
    phase = sin (phasePosition);
    return phase * amplitude;
}

void SineOscillator::setFrequency (float midiFrequency)
{
    frequency = midiFrequency;
}

void SineOscillator::setAmplitude (float veloAmplitude)
{
    amplitude = veloAmplitude;
}

void SineOscillator::setSampleRate (float recievedSampleRate)
{
    sampleRate = recievedSampleRate;
}