//
//  Counter.h
//  JuceBasicAudio
//
//  Created by Jeffrey Bateman on 11/20/14.
//
//

#ifndef __JuceBasicAudio__Counter__
#define __JuceBasicAudio__Counter__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"

class Counter    :public Thread
{
public:
    Counter();
    ~Counter();
    void reset();
    void startCounter();
    void stopCounter();
    bool getCurrentState() const;
    void run() override;
    
    class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener() {}
        /** Called when the next timer has reached the next interval. */
        virtual void counterChanged (const unsigned int counterValue) = 0;
    };
    void setListener (Listener* newListener);
    
private:
    unsigned int count;
    Listener* listener;
    int counterValue;
    
};



#endif /* defined(__JuceBasicAudio__Counter__) */
