//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Jeffrey Bateman on 11/20/14.
//
//

#include "Counter.h"
#include "math.h"

Counter::Counter() : Thread("CounterThread")
{
    listener = nullptr;
    count = 0;
}

Counter::~Counter()
{
    stopThread (500);
}

void Counter::reset()
{
    count = 0;
}
void Counter::startCounter()
{
    startThread();
}

void Counter::stopCounter()
{
    stopThread (500);
}

bool Counter::getCurrentState() const
{
    return isThreadRunning();
}

void Counter::run()
{
//    while (!threadShouldExit())
//    {
//        std::cout << "Counter:" << count << "\n";
//        ++count;
//    }
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        if (listener != nullptr)
            listener->counterChanged (count++);
        Time::waitForMillisecondCounter (time + 200);
    }
}

void Counter::setListener (Listener* newListener)
{
    listener = newListener;
}