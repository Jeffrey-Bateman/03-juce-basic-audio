//
//  SineOscillator.h
//  JuceBasicAudio
//
//  Created by Jeffrey Bateman on 11/19/14.
//
//

#ifndef __JuceBasicAudio__SinOscillator__
#define __JuceBasicAudio__SinOscillator__

#include <iostream>

class SineOscillator
{
public:
    SineOscillator(); //Constructor
    ~SineOscillator(); //Desturctor
    float getSample();
    void setFrequency (float midiFrequency); //mutator
    void setAmplitude (float veloAmplitude); // mutator
    void setSampleRate (float recievedSampleRate); //mutator
    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float twoPi;
    float phaseIncrement;
    float phasePosition;
    float wavePosition;
};

#endif /* defined(__JuceBasicAudio__SinOscillator__) */
