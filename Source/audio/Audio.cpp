/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.setMidiInputEnabled ("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    sharedMemory.enter();
    sineOsc.setFrequency (MidiMessage::getMidiNoteInHertz(message.getNoteNumber()));
    sineOsc.setAmplitude (message.getFloatVelocity());
    sharedMemory.exit();
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        float sample = sineOsc.getSample();
        float *wavePointer = &sample;
        
        *outL = *wavePointer * gain;
        *outR = *wavePointer * gain;
        
        inL++;
        inR++;
        
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    gain = 0.f;
    sineOsc.setSampleRate(device->getCurrentSampleRate());
}

void Audio::audioDeviceStopped()
{

}

void Audio::audioGainUpdate (float sliderValue)
{
    gain = pow(sliderValue, 3);
}