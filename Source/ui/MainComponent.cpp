/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)

{
    setSize (500, 400);
    
    gainSlider.setRange (0.0, 1.0);
    gainSlider.setBounds(10, 10, getWidth() - 10, 10);
    addAndMakeVisible (&gainSlider);
    gainSlider.addListener(this);
    
    textButton.setButtonText("Counter");
    textButton.setBounds(10, 50, getWidth() - 10, 30);
    textButton.addListener(this);
    addAndMakeVisible(&textButton);
    
    counter.setListener(this);

}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{

}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

//GainSlider callback===========================================================
void MainComponent::sliderValueChanged (Slider* slider)
{
    if (slider == &gainSlider)
    {
        sharedMemory.enter();
        audio.audioGainUpdate (pow(gainSlider.getValue(), 3));
        sharedMemory.exit();
    }
    
}



void MainComponent::buttonClicked (Button *button)
{
    if (button == &textButton)
    {
        if (counter.getCurrentState() == false)
        {
            counter.startCounter();
            textButton.setToggleState(false, dontSendNotification);
        }
        else if (counter.getCurrentState() == true)
        {
            counter.stopCounter();
            counter.reset();
            textButton.setToggleState(true, dontSendNotification);
        }
    }
}

void MainComponent::counterChanged (const unsigned int counterValue)
{
    std::cout << "Counter:" << counterValue << "\n";
}